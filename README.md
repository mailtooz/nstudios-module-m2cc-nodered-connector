# NStudios Node-Red Connector Module #

In order to reduce coding time and provide a flexible, robust and UI driven integration mechanism with a business
intelligence dashboard this feature will be built to leverage NodeRed iPaas (integrated platform as a service). The iPass
allows visual control over data mapping between multiple systems and endpoints. This provides quick and easy
integration with reporting and scheduling. It's system agnostic and built in business intelligence dashboard will allow
for future integrations and updates to be carried out with little or no code change dependencies.

# Orders Integration
This module uses RabbitMQ to queue newly placed orders. The Message Queue consumer triggers a NodeRed (iPaaS) webhook 
service passing the queued order ID's from the message queue

## Available Configurations:
- iPaaS URL / Credentials
- Orders Enabled
- Orders Webhook URL
- Queueable Order Statuses
