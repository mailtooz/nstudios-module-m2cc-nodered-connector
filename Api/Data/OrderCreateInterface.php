<?php
namespace NStudios\NoderedConnector\Api\Data;

/**
 * Order Create Data Interface
 *
 * @package NStudios_NoderedConnector
 * @author Ozan Yavasoglulari <support@nstudios.uk>
 * @copyright 2018 NStudios (https://www.nstudios.uk)
 */
interface OrderCreateInterface
{
    /**
     * @return string|null
     */
    public function getId();

    /**
     * @param string $id
     * @return \NStudios\NoderedConnector\Api\Data\OrderCreateInterface
     */
    public function setId(string $id);
}
