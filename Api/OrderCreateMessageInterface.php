<?php
namespace NStudios\NoderedConnector\Api;

/**
 * Order Create Message Interface
 *
 * @package NStudios_NoderedConnector
 * @author Ozan Yavasoglulari <support@nstudios.uk>
 * @copyright 2018 Rebox Digital (https://nstudios.uk)
 */
interface OrderCreateMessageInterface
{
    /**
     * @return \NStudios\NoderedConnector\Api\Data\OrderCreateInterface
     */
    public function getOrderIncrementId();

    /**
     * @param string $orderIncrementId
     * @return \NStudios\NoderedConnector\Api\OrderCreateMessageInterface
     */
    public function setOrderCreatedIncrementId(string $orderIncrementId);
}
