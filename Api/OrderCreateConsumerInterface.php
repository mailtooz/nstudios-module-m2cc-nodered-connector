<?php
namespace NStudios\NoderedConnector\Api;

/**
 * Order Create Consumer Interface
 *
 * @package NStudios_NoderedConnector
 * @author Ozan Yavasoglulari <support@nstudios.com>
 * @copyright 2018 NStudios (https://www.nstudios.com)
 */
interface OrderCreateConsumerInterface
{
    /**
     * Process order create message.
     *
     * @param \NStudios\NoderedConnector\Api\OrderCreateMessageInterface $message
     * @return \NStudios\NoderedConnector\Api\OrderCreateMessageInterface
     */
    public function consumeOrderCreate(OrderCreateMessageInterface $message);
}
