<?php
namespace NStudios\NoderedConnector\Api;

use NStudios\NoderedConnector\Api\Data\OrderCreateInterface;

/**
 * Order Publisher Interface
 *
 * @package NStudios_NoderedConnector
 * @author Ozan Yavasoglulari <support@nstudios.uk>
 * @copyright 2018 NStudios (https://www.nstudios.uk)
 */
interface OrderCreatePublisherInterface
{
    /**
     * Add saved order to message queue
     *
     * @param OrderCreateInterface $orderCreate
     */
    public function publishSavedOrder(OrderCreateInterface $orderCreate);
}
