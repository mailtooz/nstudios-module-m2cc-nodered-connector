<?php
namespace NStudios\NoderedConnector\Observer\Sales\Order;

use NStudios\NoderedConnector\Helper\Config;
use NStudios\NoderedConnector\Api\OrderCreatePublisherInterface;
use NStudios\NoderedConnector\Api\Data\OrderCreateInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;

/**
 * Sales Order Save After event to add sales order te RMQ job
 *
 * @package NStudios_NoderedConnector
 * @author Ozan Yavasoglulari <support@nstudios.com>
 * @copyright 2018 NStudios (https://www.nstudios.com)
 */
class AddToQueue implements ObserverInterface
{
    /**
     * @var \NStudios\NoderedConnector\Api\OrderCreatePublisherInterface
     */
    private $orderCreatePublisher;

    /**
     * @var \NStudios\NoderedConnector\Helper\Config
     */
    private $sysConfig;

    /**
     * @var $logger \Psr\Log\LoggerInterface;
     */
    private $logger;


    /**
     * @var \NStudios\NoderedConnector\Api\Data\OrderCreateInterface
     */
    private $orderInterface;

    public function __construct(
        LoggerInterface $logger,
        OrderCreatePublisherInterface $orderCreatePublisher,
        OrderCreateInterface $orderCreate,
        Config $sysConfig
    ) {
        $this->logger = $logger;
        $this->orderCreatePublisher = $orderCreatePublisher;
        $this->orderInterface = $orderCreate;
        $this->sysConfig = $sysConfig;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(Observer $observer)
    {
        if (!$this->sysConfig->isOrderProcessingEnabled()) {
            return false;
        }

        /**
         * @var \Magento\Sales\Api\Data\OrderInterface $oderCreatePublisher
         */
        $orderObj = $observer->getEvent()->getOrder();
        $this->orderInterface->setId($orderObj->getIncrementId());

        /**
         * If this is not a new Order then do nothing
         */
        if ($orderObj->getState() != "new") {
            return;
        }

        $serviceUrl = $this->sysConfig->getServiceUrl();
        $webhookPath = $this->sysConfig->getOrderSaveAfterWebhook();

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_PORT => "1880",
            CURLOPT_URL => $serviceUrl . $webhookPath,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "order_increment_ids=" . $orderObj->getIncrementId(),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded",
                "Postman-Token: c6c4fcc6-46af-4638-be39-59473f82482e"
            ),
        ));

        $response = curl_exec($curl);
        $errMsg = curl_error($curl);
        $errNo = curl_errno($curl);
        curl_close($curl);

        if ($errNo) {
            /**
             * If there is an error we need to requeue the order
             * so that it can be re-sent
             * $this->logger->debug("cURL Error #:" . $errMsg . ":" . $errNo);
             */
            $this->orderCreatePublisher->publishSavedOrder($this->orderInterface);
            $this->logger->debug("cURL Response: " . $response . " Error: " . $errNo . ":" . $errMsg);
        }
    }
}
