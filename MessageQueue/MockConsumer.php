<?php
namespace NStudios\NoderedConnector\MessageQueue;

use Magento\Framework\MessageQueue\ConsumerInterface;

/**
 * Class MockConsumer
 *
 * @package   NStudios_NoderedConnector
 * @author    Ozan Yavasoglulari <support@nstudios.uk>
 * @copyright 2018 NStudios
 */
class MockConsumer implements ConsumerInterface
{
    /**
     * Process queued messages.
     *
     * @param int|null $maxNumberOfMessages
     */
    public function process($maxNumberOfMessages = null)
    {
        return;
    }
}
