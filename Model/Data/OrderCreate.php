<?php
namespace NStudios\NoderedConnector\Model\Data;

use NStudios\NoderedConnector\Api\Data\OrderCreateInterface;

/**
 * Class OrderCreate
 *
 * @package NStudios_NoderedConnector
 * @author Ozan Yavasoglulari <support@nstudios.uk>
 * @copyright 2018 Rebox Digital (https://nstudios.uk)
 */
class OrderCreate implements OrderCreateInterface
{
    /**
     * @var int
     */
    private $orderId;

    /**
     * @return string|null
     */
    public function getId()
    {
        return $this->orderId;
    }

    /**
     * @param string $id
     * @return \NStudios\NoderedConnector\Api\Data\OrderCreateInterface
     */
    public function setId(string $id)
    {
        $this->orderId = $id;

        return $this;
    }
}
