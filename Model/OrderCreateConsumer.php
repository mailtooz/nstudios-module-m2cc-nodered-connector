<?php
namespace NStudios\NoderedConnector\Model;

use NStudios\NoderedConnector\Api\OrderCreateConsumerInterface;
use NStudios\NoderedConnector\Api\OrderCreateMessageInterface;

/**
 * Class OrderCreateConsumer
 *
 * @package NStudios_NoderedConnector
 * @author Ozan Yavasoglulari <support@nstudios.com>
 * @copyright 2018 NStudios (https://www.nstudios.com)
 */
class OrderCreateConsumer implements OrderCreateConsumerInterface
{
    const CONSUMER_NAME = 'orderCreateConsumer';

    /**
     * @param \NStudios\NoderedConnector\Api\OrderCreateMessageInterface $message
     * @return \NStudios\NoderedConnector\Api\OrderCreateMessageInterface
     */
    public function consumeOrderCreate(OrderCreateMessageInterface $message)
    {
        return $message;
    }
}
