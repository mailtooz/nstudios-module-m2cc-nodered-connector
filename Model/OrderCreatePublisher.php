<?php
namespace NStudios\NoderedConnector\Model;

use NStudios\NoderedConnector\Api\OrderCreatePublisherInterface;
use NStudios\NoderedConnector\Api\Data\OrderCreateInterface;
use Magento\Framework\MessageQueue\PublisherInterface;

/**
 * Class OrderCreatePublisher
 *
 * @package NStudios_NoderedConnector
 * @author Ozan Yavasoglulari <support@nstudios.com>
 * @copyright 2018 NStudios (https://www.nstudios.com)
 */
class OrderCreatePublisher implements OrderCreatePublisherInterface
{
    const MSG_TOPIC_NAME = 'nstudios.nodered.connector.order.created';

    /*
     * @var \Magento\Framework\MessageQueue\PublisherInterface
     */
    private $msgQueuePublisher;

    /**
     * OrderCreatePublisher constructor.
     * @param \Magento\Framework\MessageQueue\PublisherInterface $publisher
     */
    public function __construct(
        PublisherInterface $publisher
    ) {
        $this->msgQueuePublisher = $publisher;
    }

    /**
     * Add saved order to message queue
     *
     * @param OrderCreateInterface $orderCreate
     */
    public function publishSavedOrder(OrderCreateInterface $orderCreate)
    {
        $this->msgQueuePublisher->publish(self::MSG_TOPIC_NAME, $orderCreate->getId());
    }
}
