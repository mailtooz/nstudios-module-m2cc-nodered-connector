<?php
namespace NStudios\NoderedConnector\Model;

use NStudios\NoderedConnector\Api\OrderCreateMessageInterface;
use NStudios\NoderedConnector\Api\Data\OrderCreateInterface;

/**
 * Class OrderCreateMessage
 *
 * @package NStudios_NoderedConnector
 * @author Ozan Yavasoglulari <support@nstudios.com>
 * @copyright 2018 NStudios (https://www.nstudios.com)
 */
class OrderCreateMessage implements OrderCreateMessageInterface
{
    /**
     * @var \NStudios\NoderedConnector\Api\Data\OrderCreateInterface
     */
    private $orderIncrementId;

    /**
     * return string
     */
    public function getOrderIncrementId()
    {
        return $this->orderIncrementId;
    }


    /**
     * @param string $orderIncrementId
     * @return \NStudios\NoderedConnector\Api\OrderCreateMessageInterface
     * @author Ozan Yavasoglulari <support@nstudios.uk>
     */
    public function setOrderCreatedIncrementId(string $orderIncrementId)
    {
        $this->orderIncrementId = $orderIncrementId;
        return $this;
    }
}
