<?php
namespace NStudios\NoderedConnector\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use NStudios\NoderedConnector\Helper\Config;

class Product extends AbstractHelper
{

    /**
     * @var \NStudios\NoderedConnector\Helper\Config
     */
    private $sysConfig;

    /**
     * Product constructor.
     * @param Context $context
     * @param \NStudios\NoderedConnector\Helper\Config $sysConfig
     */
    public function __construct(
        Context $context,
        Config $sysConfig
    ) {
        $this->sysConfig = $sysConfig;
        parent::__construct($context);
    }

    /**
     * Returns product data from nodered
     *
     * @param $sku
     * @return mixed
     */
    public function getProductData($sku)
    {
        if (!$this->sysConfig->isProductProcessingEnabled() || empty($sku)) {
            return false;
        }

        $serviceUrl = $this->sysConfig->getServiceUrl();
        $webhook = $this->sysConfig->getProductGetWebhook();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $serviceUrl . $webhook);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "sku=" . strval($sku));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);

        if (!$result || curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200) {
            $result = false;
        }

        curl_close($ch);
        return $result;
    }
}
